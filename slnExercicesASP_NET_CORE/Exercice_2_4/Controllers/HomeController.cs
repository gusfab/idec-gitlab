﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercice_2_4.Models;
using Microsoft.AspNetCore.Mvc;

namespace Exercice_2_4.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Employee()
        {

            Employee emp1 = new Employee
            {
                EmployyId = 1,
                Name = "Gustavo Fabrizio",
                Designation = "Web Developper"
            };

            ViewBag.Company = "Google Inc";
            ViewData["CompanyLocation"] = "United States";

            return View(emp1);
        }
    }
}