﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Exercice_2_1.Controllers
{
    public class HomeController : Controller
    {
        //2. Ajouter un contrôleur "Home" avec une méthode "Index" qui retourne le message : "Hello World! I am learning MVC! "
        public string Index()
        {
            return "Hello World ! I'm learning MVC !";
        }


    }
}