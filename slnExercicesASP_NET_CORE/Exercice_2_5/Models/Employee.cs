﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercice_2_5.Models
{
    public class Employee
    {

        public int EmployyId { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
    }
}
