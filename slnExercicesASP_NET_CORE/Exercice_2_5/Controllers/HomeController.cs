﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercice_2_5.Filters;
using Exercice_2_5.Models;
using Microsoft.AspNetCore.Mvc;

namespace Exercice_2_5.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [SundayFilter]
        public IActionResult Employee()
        {

            Employee emp1 = new Employee
            {
                EmployyId = 1,
                Name = "Gustavo Fabrizio",
                Designation = "Web Developper"
            };

            ViewBag.Company = "Google Inc";
            ViewData["CompanyLocation"] = "United States";

            return View(emp1);
        }
    }
}