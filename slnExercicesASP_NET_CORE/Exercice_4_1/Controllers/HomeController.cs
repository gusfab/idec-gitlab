﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercice_4_1.Models;
using Microsoft.AspNetCore.Mvc;

namespace Exercice_4_1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            List<Product> Products = new List<Product>
            {
                new Product { Name = "Mobile Phone", Price = 300 },
                new Product { Name = "Laptop", Price = 1000 },
                new Product { Name = "Tablet", Price = 600 }
            };

            Order order = new Order();

            order.Products = Products;

            OrderViewModel viewModel = new OrderViewModel(order);

            return View(viewModel);
        }
    }
}