﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercice_4_1.Models
{
    public class OrderViewModel
    {
        public OrderViewModel(Order order)
        {
            Products = order.Products;
            Total = order.Products.Sum(product => product.Price);
        }

        public decimal Total { get; set; }
        public List<Product> Products { get; set; }
        public decimal Discount => Total > 1000M ? Total * 0.1M : Total;
    }
}
