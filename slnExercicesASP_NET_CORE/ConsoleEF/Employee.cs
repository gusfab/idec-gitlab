﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEF
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
        public string Designation { get; set; }

        public override string ToString()
        {
            return "EmployeeId = " + EmployeeId +
                "; Name = " + Name +
                "; Salary = " + Salary +
                "; Designation = " + Designation;

        }
    }

}
