﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice_2_2.Controllers
{
    public class UpperStringActionResult : ActionResult
    {
        string _str;

        public UpperStringActionResult(string str)
        {
            _str = str;
        }

        public override void ExecuteResult(ActionContext context)
        {
            var upperStringBytes = Encoding.UTF8.GetBytes(_str.ToUpper());

            context.HttpContext.Response.Body.Write(upperStringBytes, 0, upperStringBytes.Length);
        }
    }
}
