﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductViewer.Models;

namespace ProductViewer.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
            => View(ProductRepository.SharedRepository.Products);
    }
}