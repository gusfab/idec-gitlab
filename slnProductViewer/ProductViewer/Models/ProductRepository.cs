﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductViewer.Models
{
    public class ProductRepository
    {
        private static ProductRepository sharedRepository = new ProductRepository();
        private Dictionary<string, Product> products = new Dictionary<string, Product>();
        public static ProductRepository SharedRepository => sharedRepository;
        public ProductRepository()
        {
            var initialItems = new[]
            {
                new Product { Name = "Kayak", Price = 275M },
                new Product { Name = "Life Jacket", Price = 48.95M },
                new Product { Name = "Soccer Ball", Price = 19.50M },
                new Product { Name = "Corner Flag", Price = 34.95M },
            };

            foreach (var p in initialItems)
            {
                AddProduct(p);
            }
        }

        public IEnumerable<Product> Products => products.Values;
        public void AddProduct(Product p) => products.Add(p.Name, p);
    }
}
