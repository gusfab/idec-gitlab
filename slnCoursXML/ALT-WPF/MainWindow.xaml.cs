﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace ALT_WPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            XmlCrud.CreateXmlDocument();
            MessageBox.Show("Document créé");
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var xDoc = XDocument.Load("c:\\test2.xml");
            
            foreach (var elem in xDoc.Document.Descendants("Dechets"))
            {
                foreach (var attr in elem.Attributes("date"))
                {
                    if (attr.Value.Equals(lstDates.SelectedItem.ToString()))
                        elem.RemoveAll();
                }
            }

            //xDoc.Elements("Dechets").Where(x => x.Attribute("date").Value == lstDates.SelectedItem.ToString()).FirstOrDefault().Remove();
            /*
            XmlDocument doc = new XmlDocument();
            doc.Load("c:\\test2.xml");
                        
            XmlNode dechetsNode = doc.SelectSingleNode("/Calendrier/Dechets[@date]");
            if(dechetsNode != null)
            {
                XmlNode parent = dechetsNode.ParentNode;
                parent.RemoveChild(dechetsNode);
                doc.Save("c:\\test2.xml");
            }*/


            xDoc.Save("c:\\test2.xml");
            MessageBox.Show("Donnée supprimée");
        }

        private void btnRead_Click(object sender, RoutedEventArgs e)
        {
            /* OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                tbxData.Text = File.ReadAllText(openFileDialog.FileName); */

        }

        private void btnNewEntry_Click(object sender, RoutedEventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("c:\\test2.xml");

            XmlElement dechets = doc.CreateElement("Dechets");
            doc.DocumentElement.AppendChild(dechets);
            XmlAttribute date = doc.CreateAttribute("date");
            dechets.Attributes.Append(date);
            date.Value = dpkDate.SelectedDate.ToString();
            XmlElement denomination = doc.CreateElement("Denomination");
            XmlElement poids = doc.CreateElement("Poids");



            XmlElement metaux = doc.CreateElement("Metaux");
            dechets.AppendChild(metaux);
            XmlAttribute typeMetal = doc.CreateAttribute("type");
            typeMetal.Value = "métal";
            metaux.Attributes.Append(typeMetal);
            XmlElement denominationMetaux = doc.CreateElement("Denomination");
            XmlElement poidsMetaux = doc.CreateElement("Poids");
            metaux.AppendChild(denominationMetaux);
            metaux.AppendChild(poidsMetaux);
            denominationMetaux.InnerText = txbMetaux.Text;
            poidsMetaux.InnerText = tbxPoidsMetaux.Text;

            XmlElement verts = doc.CreateElement("Verts");
            dechets.AppendChild(verts);
            XmlAttribute typeVerts = doc.CreateAttribute("type");
            typeVerts.Value = "vert";
            verts.Attributes.Append(typeVerts);
            XmlElement denominationVerts = doc.CreateElement("Denomination");
            XmlElement poidsVerts = doc.CreateElement("Poids");
            verts.AppendChild(denominationVerts);
            verts.AppendChild(poidsVerts);
            denominationVerts.InnerText = txbVerts.Text;
            poidsVerts.InnerText = tbxPoidsVerts.Text;


            XmlElement PET = doc.CreateElement("PET");
            dechets.AppendChild(PET);
            XmlAttribute typePET = doc.CreateAttribute("type");
            typePET.Value = "PET";
            PET.Attributes.Append(typePET);
            XmlElement denominationPET = doc.CreateElement("Denomination");
            XmlElement poidsPET = doc.CreateElement("Poids");
            PET.AppendChild(denominationPET);
            PET.AppendChild(poidsPET);
            denominationPET.InnerText = txbPET.Text;
            poidsPET.InnerText = tbxPoidsPET.Text;

            XmlElement encombrants = doc.CreateElement("Encombrants");
            dechets.AppendChild(encombrants);
            XmlAttribute typeEncombrants = doc.CreateAttribute("type");
            typeEncombrants.Value = "encombrant";
            encombrants.Attributes.Append(typeEncombrants);
            XmlElement denominationEncombrants = doc.CreateElement("Denomination");
            XmlElement poidsEncombrants = doc.CreateElement("Poids");
            encombrants.AppendChild(denominationEncombrants);
            encombrants.AppendChild(poidsEncombrants);
            denominationEncombrants.InnerText = txbEncombrants.Text;
            poidsEncombrants.InnerText = tbxPoidsEncombrants.Text;

            XmlElement bois = doc.CreateElement("Bois");
            dechets.AppendChild(bois);
            XmlAttribute typeBois = doc.CreateAttribute("type");
            typeBois.Value = "bois";
            bois.Attributes.Append(typeBois);
            XmlElement denominationBois = doc.CreateElement("Denomination");
            XmlElement poidsBois = doc.CreateElement("Poids");
            bois.AppendChild(denominationBois);
            bois.AppendChild(poidsBois);
            denominationBois.InnerText = txbBois.Text;
            poidsBois.InnerText = tbxPoidsBois.Text;

            doc.Save("c:\\test2.xml");

            MessageBox.Show("Nouvelle entrée ajoutée");
        }
    }
}
