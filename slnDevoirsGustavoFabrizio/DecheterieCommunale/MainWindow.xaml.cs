﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace DecheterieCommunale
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void newEntry()
        {
            XmlDocument doc = new XmlDocument();


            XmlElement rootNode = doc.CreateElement("Dechets");
            doc.DocumentElement.AppendChild(rootNode);
            XmlAttribute date = doc.CreateAttribute("date");
            rootNode.Attributes.Append(date);
            date.Value = DateTime.Today.ToString();

            XmlElement subNode = doc.CreateElement("Dechet");
            rootNode.AppendChild(subNode);
            XmlAttribute subNodeAttributes = doc.CreateAttribute("type", "poids");
            subNode.Attributes.Append(subNodeAttributes);
        }
    }
}
