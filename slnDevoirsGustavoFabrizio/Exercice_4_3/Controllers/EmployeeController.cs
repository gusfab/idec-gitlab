﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercice_4_3.Models;
using Exercice_4_3.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Exercice_4_3.Controllers
{
    public class EmployeeController : Controller
    {
        readonly EmployeeDbContext employeeDbContext;
        public EmployeeController(EmployeeDbContext employeeDbContext)
        {
            this.employeeDbContext = employeeDbContext;
        }
        // GET: /<controllers>/
        
        public IActionResult Index()
        {
            EmployeeAddViewModel employeeAddViewModel = new EmployeeAddViewModel();
            var db = this.employeeDbContext;
            employeeAddViewModel.EmployeeList = db.Employees.ToList();
            employeeAddViewModel.NewEmployee = new Employee();

            return View(employeeAddViewModel);
        }
        

        [HttpPost]
        public IActionResult Index(EmployeeAddViewModel employeeAddViewModel)
        {
            if (ModelState.IsValid)
            {
                Employee newEmployee = new Employee
                {
                    Name = employeeAddViewModel.Name,
                    Designation = employeeAddViewModel.Designation,
                    Salary = employeeAddViewModel.Salary
                };

                employeeDbContext.Employees.Add(newEmployee);
                employeeDbContext.SaveChanges();

                //Redirect to get Index GET method
                return RedirectToAction("Index");
            }

            employeeAddViewModel.EmployeeList = employeeDbContext.Employees.ToList();
            return View(employeeAddViewModel);
            /*
            var db = this.employeeDbContext;
            db.Employees.Add(employeeAddViewModel.NewEmployee);
            db.SaveChanges();

            //Redirect to get Index GET method
            return RedirectToAction("Index");
            */
        }
    }
}