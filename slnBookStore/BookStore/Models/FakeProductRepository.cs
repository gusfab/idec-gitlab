﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models
{
    public class FakeProductRepository : IProductRepository
    {
        public IQueryable<Product> Products => new List<Product> { 
            new Product { Name = "Design Patterns", Price = 25},
            new Product { Name = "IPad Pro", Price = 179},
            new Product { Name = "Basket de course", Price = 95}
            }.AsQueryable<Product>();
    }
}
