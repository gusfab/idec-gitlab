﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices
            .GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();

            if (!context.Products.Any())
            {
                context.Products.AddRange(
                    new Product
                    {
                        Name = "Computer Science: An Interdisciplinary Approach",
                        Description = "Une description du livre",
                        Category = "Books",
                        Price = 275
                    },
                    new Product
                    {
                        Name = "Computer Science Distilled: Learn the Art of Solving Computational Problems",
                        Description = "Une description du livre",
                        Category = "Books",
                        Price = 48.95m
                    },
                    new Product
                    {
                        Name = "Code: The Hidden Language of Computer Hardware and Software",
                        Description = "Une description du livre",
                        Category = "Books",
                        Price = 19.50m
                    },
                    new Product
                    {
                        Name = "The Self-Taught Programmer: The Definitive Guide to Programming Professionally",
                        Description = "Une description du livre",
                        Category = "Books",
                        Price = 34.95m
                    },
                    new Product
                    {
                        Name = "Algorithms to Live By: The Computer Science of Human Decisions",
                        Description = "Une description du livre",
                        Category = "Books",
                        Price = 79500
                    },
                    new Product
                    {
                        Name = "Wyze Cam 1080p HD Indoor Wireless Smart Home Camera",
                        Description = "Une description du produit",
                        Category = "Electronics",
                        Price = 16
                    },
                    new Product
                    {
                        Name = "Samsung 128GB 100MB/s (U3) MicroSDXC EVO",
                        Description = "Une description du produit",
                        Category = "Electronics",
                        Price = 29.95m
                    },
                    new Product
                    {
                        Name = "AmazonBasics High-Speed 4K HDMI Cable",
                        Description = "Une description du produit",
                        Category = "Electronics",
                        Price = 75
                    },
                    new Product
                    {
                        Name = "Infant Optics DXR-8 Video Baby Monitor",
                        Description = "Une description du produit",
                        Category = "Electronics",
                        Price = 1200
                    });
            }
            context.SaveChanges();


        }
    }
}
