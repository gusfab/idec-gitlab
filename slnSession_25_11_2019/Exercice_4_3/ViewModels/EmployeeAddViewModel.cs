﻿using Exercice_4_3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercice_4_3.ViewModels
{
    public class EmployeeAddViewModel
    {
        public List<Employee> EmployeeList { get; set; }
        public Employee NewEmployee { get; set; }
    }
}
