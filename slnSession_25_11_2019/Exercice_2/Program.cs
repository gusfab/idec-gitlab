﻿using System;
using System.Linq;

namespace Exercice_2
{
    class Program
    {
        static void Main(string[] args)
        {
            AddEmployee();
        }

        static void AddEmployee()
        {
            using (var db = new EmployeeDbContext())
            {
                Employee employee = new Employee
                {
                    Designation = "Web AppDev",
                    Name = "Bill",
                    Salary = 6800
                };

                db.Employees.Add(employee);
                int recordsInserted = db.SaveChanges();
                Console.WriteLine("Number of records inserted: " + recordsInserted);

                Employee employeeFromDB = db.Employees.Where(emp => emp.EmployeeID == 2).FirstOrDefault();
                Console.WriteLine(employeeFromDB);
                Console.ReadLine();
            }
        }

        static void UpdateSalary()
        {
            using (var db = new EmployeeDbContext())
            {
                Employee employee = db.Employees.Where(emp => emp.EmployeeID == 1).FirstOrDefault();
                if (employee != null)
                {
                    employee.Salary = 6500;
                    int recordsUpdated = db.SaveChanges();
                    Console.WriteLine("Records updated: " + recordsUpdated);
                    Console.ReadLine();
                }
            }
        }

        static void DeleteEmployee()
        {
            using (var db = new EmployeeDbContext())
            {
                Employee employeeToBeDeleted = db.Employees.Where(emp => emp.EmployeeID == 1).FirstOrDefault();
                if (employeeToBeDeleted != null)
                {
                    db.Entry(employeeToBeDeleted).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    int recordsDeleted = db.SaveChanges();
                    Console.WriteLine("Number of records deleted:" + recordsDeleted);
                    Console.ReadLine();
                }
            }
        }
    }
}
