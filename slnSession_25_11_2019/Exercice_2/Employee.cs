﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercice_2
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
        public string Designation { get; set; }
        public override string ToString()
        {
            return "EmployeeID = " + EmployeeID +
                   "; Name = " + Name +
                   "; Salary = " + Salary +
                   "; Designation = " + Designation;
        }
    }
}
