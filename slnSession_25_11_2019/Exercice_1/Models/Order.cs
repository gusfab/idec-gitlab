﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercice_1.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public List<Product> Products { get; set; }
        public decimal Total { get; set; }
    }
}
