﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Villes.Models;

namespace Villes.Controllers
{
    public class HomeController : Controller
    {
        private IRepository repository;

        public HomeController(IRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index() => View(repository.Villes);
        public ViewResult Create() => View();

        [HttpPost]

        public IActionResult Create(Ville ville)
        {
            repository.AddVille(ville);
            return RedirectToAction("Index");
        }
        /*public IActionResult Index()
        {
            return View();
        }*/
    }
}