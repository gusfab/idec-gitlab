﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Villes.Models
{
    public interface IRepository
    {
        IEnumerable<Ville> Villes { get; }
        void AddVille(Ville newVille);
    }
}
