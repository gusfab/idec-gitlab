﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Villes.Models
{
    public class MemoryRepository : IRepository
    {
        private List<Ville> villes = new List<Ville>
        {
            new Ville { Name = "London", Country = "UK", Population = 8539000},
            new Ville { Name = "New York", Country = "USA", Population = 8406000},
            new Ville { Name = "San Jose", Country = "USA", Population = 998537},
            new Ville { Name = "Sierre", Country = "Switzerland", Population = 15000}
        };

        public IEnumerable<Ville> Villes => villes;

        public void AddVille(Ville newVille)
        {
            villes.Add(newVille);
        }
    }
}
